import datetime

import pytest


@pytest.mark.parametrize("serial_plugin", ["msgpack", "json", "str"])
def test_dump_load(hub, serial_plugin):
    """
    Verify that standard types can be dumped and loaded without change
    """
    data = {"dict": {}, "list": [], "none": None, "": "", "int": 1, "float": 1.2}
    byte_data = hub.serial[serial_plugin].dump(data)
    deserialized = hub.serial[serial_plugin].load(byte_data)
    assert data == deserialized


@pytest.mark.parametrize("serial_plugin", ["msgpack", "json", "str"])
def test_obj(hub, serial_plugin):
    data = object()
    byte_data = hub.serial[serial_plugin].dump(data)
    deserialized = hub.serial[serial_plugin].load(byte_data)
    assert repr(data) == deserialized


@pytest.mark.parametrize("serial_plugin", ["msgpack", "json", "str"])
def test_dump(hub, serial_plugin):
    """
    Verify that unusual types don't throw errors on dump
    """

    def _callable():
        ...

    data = [
        None,
        object(),
        1,
        "string",
        1j + 1,
        datetime.datetime.now(),
        _callable,
        ...,
        lambda: ...,
        Exception,
    ]
    assert hub.serial[serial_plugin].dump(data)
